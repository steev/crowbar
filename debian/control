Source: crowbar
Section: misc
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3-all,
               python3-setuptools
Standards-Version: 4.6.1.0
Homepage: https://github.com/galkan/crowbar
Vcs-Git: https://gitlab.com/kalilinux/packages/crowbar.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/crowbar

Package: crowbar
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, openvpn, freerdp2-x11, vncviewer,
 python3-paramiko, python3-nmap
Description: Brute forcing tool
 This package contains Crowbar (formally known as Levye). It is a brute forcing
 tool that can be used during penetration tests. It was developed to brute force
 some protocols in a different manner according to other popular brute forcing
 tools. As an example, while most brute forcing tools use username and password
 for SSH brute force, Crowbar uses SSH key(s). This allows for any private keys
 that have been obtained during penetration tests, to be used to attack other
 SSH servers.
 .
 Currently Crowbar supports:
    * OpenVPN (-b openvpn)
    * Remote Desktop Protocol (RDP) with NLA support (-b rdp)
    * SSH private key authentication (-b sshkey)
    * VNC key authentication (-b vpn)
